﻿#include"pch.h"
#include <glad.h>
#include <glfw3.h>

#define GLM_FORCE_CTOR_INIT 
#include <GLM.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include <shader_m.h>
#include"FileSystem.h"
#include "Model.h"
#include"Mesh.h"
#include"Camera1.h"
#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
bool gammaEnabled = false;
// camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;
unsigned int squareVAO = 0;
void renderSquare()
{
	unsigned int squareVBO;

	if (squareVAO == 0) {
		float squareVertices[] = {

			2.5f, 2.5f,  0.0f,    1.0f,  0.0f,
			5.0f, 2.5f,  0.0f,     0.0f,  0.0f,
			2.5f, 5.0f, 0.0f,    0.0f, 1.0f,

			2.5f, 5.0f,  0.0f,  1.0f,  0.0f,
			5.0f, 5.0f, 0.0f,  0.0f, 1.0f,
			5.0f, 2.5f, 0.0f,  1.0f, 1.0f
		};


		// plane VAO
		glGenVertexArrays(1, &squareVAO);
		glGenBuffers(1, &squareVBO);
		glBindVertexArray(squareVAO);
		glBindBuffer(GL_ARRAY_BUFFER, squareVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(squareVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}
unsigned int loadTexture(char const * path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT); // for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. Due to interpolation it takes texels from next repeat 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}
unsigned int CreateTexture(const std::string& strTexturePath)
{
	unsigned int textureId = -1;

	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	//stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load(strTexturePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		// set the texture wrapping parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
		// set texture filtering parameters
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else {
		std::cout << "Failed to load texture: " << strTexturePath << std::endl;
	}
	stbi_image_free(data);

	return textureId;
}


int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}

	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Muzeu", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	// configure global opengl state
   // -----------------------------
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//unsigned int sqareTexture = CreateTexture(strExePath + "\\ColoredFloor.png");
	unsigned int sqareTexture = loadTexture("ColoredFloor.png");
	// configure global opengl state
	// -----------------------------
	 // configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile shaders
	// -------------------------
	Shader shader("mapping.vs", "mapping.fs");
	Shader simpleDepthShader("mapping_depth.vs", "mapping_depth.fs");
	Shader debugDepthQuad("debug_quad.vs", "debug_quad.fs");

	Shader ourShader("lighting.vs", "lighting.fs");
	//Shader ourShader("1.model_loading.vs", "1.model_loading.fs");

	ourShader.setInt("texture_diffuse1", 0);
	glm::vec3 lightPositions[] = {
		glm::vec3(-1.7f, -1.0f, 3.0f),
	   glm::vec3(0.0f, 0.0f, 3.0f),
	   glm::vec3(-1.0f, -1.0f, 0.0f),
	   glm::vec3(1.0f, -1.0f, 0.0f),
	   glm::vec3(3.0f, -1.85f, 7.0f)
	};
	glm::vec3 lightColors[] = {
		glm::vec3(35.25),
		glm::vec3(35.25),
		glm::vec3(35.25),
		glm::vec3(35.25)
	};


	const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	// create depth texture
	unsigned int depthMap;
	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// load models
	// -----------
	Model room1("resources/objects/room/factory_building_2.obj");
	Model room2("resources/objects/room/factory_building_2.obj");
	Model angel("resources/objects/angel/Monument_angels.obj");
	Model rex("resources/objects/rex/TrexByJoel3d.FBX");
	Model statue("resources/objects/statue/12330_Statue_v1_L2.obj");
	Model liber("resources/objects/LibertyStatue/LibertStatue.obj");
	Model paint1("resources/objects/paint/obj/objPainting.obj");
	Model paint2("resources/objects/paint2/Frame5setOBJ.obj");
	Model ganditor("resources/objects/ganditorul/12335_The_Thinker_v3_l2.obj");
	Model cart("resources/objects/cart/20379_Cape_cart_v1 Textured.obj");
	// draw in wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	renderSquare();

	shader.use();
	shader.setInt("diffuseTexture", 0);
	shader.setInt("shadowMap", 1);
	debugDepthQuad.use();
	debugDepthQuad.setInt("depthMap", 0);

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		// -----
		processInput(window);

		// render
		// ------
		//glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// don't forget to enable shader before setting uniforms
		ourShader.use();



		// view/projection transformations
		glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
		glm::mat4 view = camera.GetViewMatrix();
		ourShader.setMat4("projection", projection);
		ourShader.setMat4("view", view);

		glUniform3fv(glGetUniformLocation(ourShader.ID, "lightPositions"), 4, &lightPositions[0][0]);
		glUniform3fv(glGetUniformLocation(ourShader.ID, "lightColors"), 4, &lightColors[0][0]);
		ourShader.setVec3("viewPos", camera.Position);
		ourShader.setInt("gamma", gammaEnabled);

		glm::mat4 model = glm::mat4(1.0f);
		glBindVertexArray(squareVAO);
		glBindTexture(GL_TEXTURE_2D, sqareTexture);
		model = glm::mat4(5.0f);
		model = glm::translate(model, glm::vec3(10.0f, -1.35f, 0.0f));
		ourShader.setMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glm::mat4 model1 = glm::mat4(1.0f);
		
		model1 = glm::translate(model1, glm::vec3(0.0f, -1.35f, 0.0f)); // translate it down so it's at the center of the scene
		model1 = glm::scale(model1, glm::vec3(0.02f, 0.02f, 0.02f));	// it's a bit too big for our scene, so scale it down
		ourShader.setMat4("model", model1);
		room1.Draw(ourShader);

		glm::mat4 model2 = glm::mat4(1.0f);
		model2= glm::translate(model2, glm::vec3(0.0f, -1.45f, 1.70f)); // translate it down so it's at the center of the scene
		model2 = glm::scale(model2, glm::vec3(0.02f, 0.02f, 0.02f));	// it's a bit too big for our scene, so scale it down
		model2 = glm::rotate(model2, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.setMat4("model", model2);
		room2.Draw(ourShader);

		glm::mat4 angelM = glm::mat4(1.0f);

		angelM = glm::translate(angelM, glm::vec3(10.0f, -1.35f, -9.0f)); // translate it down so it's at the center of the scene
		angelM = glm::scale(angelM, glm::vec3(0.009f, 0.009f, 0.009f));	// it's a bit too big for our scene, so scale it down
		ourShader.setMat4("model", angelM);
		angel.Draw(ourShader);

		glm::mat4 rexM = glm::mat4(1.0f);

		rexM = glm::translate(rexM, glm::vec3(3.0f, -1.25f, -7.0f)); // translate it down so it's at the center of the scene
		rexM = glm::scale(rexM, glm::vec3(0.3f, 0.3f, 0.3f));	// it's a bit too big for our scene, so scale it down
		rexM = glm::rotate(rexM, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.setMat4("model", rexM);
		rex.Draw(ourShader);

		glm::mat4 statueM = glm::mat4(1.0f);

		statueM = glm::translate(statueM, glm::vec3(11.0f, -1.25f, -5.5f)); // translate it down so it's at the center of the scene
		statueM = glm::scale(statueM, glm::vec3(0.003f, 0.003f, 0.003f));	// it's a bit too big for our scene, so scale it down
		statueM = glm::rotate(statueM, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		statueM = glm::rotate(statueM, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		ourShader.setMat4("model", statueM);
		statue.Draw(ourShader);

		glm::mat4 liberM = glm::mat4(1.0f);

		liberM = glm::translate(liberM, glm::vec3(9.5f, -1.25f, -2.5f)); // translate it down so it's at the center of the scene
		liberM = glm::scale(liberM, glm::vec3(1.8f, 1.8f, 1.8f));	// it's a bit too big for our scene, so scale it down
		liberM = glm::rotate(liberM, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.setMat4("model", liberM);
		liber.Draw(ourShader);

		

		glm::mat4 paintM = glm::mat4(1.0f);

		paintM = glm::translate(paintM, glm::vec3(-1.5f, -1.2f, 6.5f)); // translate it down so it's at the center of the scene
		paintM = glm::scale(paintM, glm::vec3(0.09f, 0.09f, 0.09f));	// it's a bit too big for our scene, so scale it down
		//paintM = glm::rotate(paintM, glm::radians(180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.setMat4("model", paintM);
		paint2.Draw(ourShader);

		glm::mat4 paintM1 = glm::mat4(1.0f);

		paintM1 = glm::translate(paintM1, glm::vec3(3.5f, -1.0f, 9.0f)); // translate it down so it's at the center of the scene
		paintM1 = glm::scale(paintM1, glm::vec3(0.3f, 0.3f, 0.3f));	// it's a bit too big for our scene, so scale it down
		paintM1 = glm::rotate(paintM1, glm::radians(-90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		ourShader.setMat4("model", paintM1);
		paint1.Draw(ourShader);

		glm::mat4 gandM = glm::mat4(1.0f);

		gandM = glm::translate(gandM, glm::vec3(-1.5f, -1.30f, 11.0f)); // translate it down so it's at the center of the scene
		gandM = glm::scale(gandM, glm::vec3(0.006f, 0.006f, 0.006f));	// it's a bit too big for our scene, so scale it down
		gandM = glm::rotate(gandM, glm::radians(-180.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		gandM = glm::rotate(gandM, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		ourShader.setMat4("model", gandM);
		ganditor.Draw(ourShader);

		glm::mat4 cartdM = glm::mat4(1.0f);

		cartdM = glm::translate(cartdM, glm::vec3(-9.5f, -1.30f, 8.0f)); // translate it down so it's at the center of the scene
		cartdM = glm::scale(cartdM, glm::vec3(0.007f, 0.007f, 0.007f));	// it's a bit too big for our scene, so scale it down
		cartdM = glm::rotate(cartdM, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		cartdM = glm::rotate(cartdM, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		ourShader.setMat4("model", cartdM);
		cart.Draw(ourShader);

		//// render the loaded model bump TheThinker_Bump.jpg -bm 0.5
		//glm::mat4 model = glm::mat4(1.0f);
		//model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
		//model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// it's a bit too big for our scene, so scale it down
		//model = glm::rotate(model, glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
		//ourShader.setMat4("model", model);
		//ourModel.Draw(ourShader);
		
		


		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, deltaTime);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}
