unsigned int squareVAO = 0;
void renderSquare()
{
	unsigned int squareVBO;

	if (squareVAO == 0) {
		float squareVertices[] = {

			2.5f, 2.5f,  0.0f,    1.0f,  0.0f,
			5.0f, 2.5f,  0.0f,     0.0f,  0.0f,
			2.5f, 5.0f, 0.0f,    0.0f, 1.0f,

			2.5f, 5.0f,  0.0f,  1.0f,  0.0f,
			5.0f, 5.0f, 0.0f,  0.0f, 1.0f,
			5.0f, 2.5f, 0.0f,  1.0f, 1.0f
		};


		// plane VAO
		glGenVertexArrays(1, &squareVAO);
		glGenBuffers(1, &squareVBO);
		glBindVertexArray(squareVAO);
		glBindBuffer(GL_ARRAY_BUFFER, squareVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(squareVertices), squareVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glBindVertexArray(0);
	}

	glBindVertexArray(grassVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}
int main(int argc, char** argv)
{
	std::string strFullExeFileName = argv[0];
	std::string strExePath;
	const size_t last_slash_idx = strFullExeFileName.rfind('\\');
	if (std::string::npos != last_slash_idx) {
		strExePath = strFullExeFileName.substr(0, last_slash_idx);
	}


	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Lab 9", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	glewInit();

	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Floor vertices

	// Floor VAO si VBO


	// Grass vertices


	// Grass VAO si VBO


	// Floor texture
	unsigned int floorTexture = CreateTexture(strExePath + "\\ColoredFloor.png");

	// Grass texture
	unsigned int grassTexture = CreateTexture(strExePath + "\\grass3.png");
	unsigned int sqareTexture = CreateTexture(strExePath + "\\ColoredFloor.png");
	// Create camera
	pCamera = new Camera(SCR_WIDTH, SCR_HEIGHT, glm::vec3(0.0, 0.0, 3.0));

	Shader shaderBlending("Blending.vs", "Blending.fs");
	shaderBlending.SetInt("texture1", 0);
	renderGrass();
	renderFloor();
	renderSquare();
	// render loop
	while (!glfwWindowShouldClose(window)) {
		// per-frame time logic
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		// input
		processInput(window);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		shaderBlending.Use();
		glm::mat4 projection = pCamera->GetProjectionMatrix();
		glm::mat4 view = pCamera->GetViewMatrix();
		shaderBlending.SetMat4("projection", projection);
		shaderBlending.SetMat4("view", view);

		glm::mat4 model;


		// Draw floor
		glBindVertexArray(floorVAO);
		glBindTexture(GL_TEXTURE_2D, floorTexture);
		model = glm::mat4();
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindVertexArray(squareVAO);
		glBindTexture(GL_TEXTURE_2D, sqareTexture);
		model = glm::mat4(5.0f);
		shaderBlending.SetMat4("model", model);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindVertexArray(grassVAO);
		glBindTexture(GL_TEXTURE_2D, grassTexture);
		if (0) {
			glBindVertexArray(grassVAO);
			glBindTexture(GL_TEXTURE_2D, grassTexture);
			model = glm::mat4(1.0);
			model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
			DrawBrunchOfGrass(shaderBlending, model);
			/*shaderBlending.SetMat4("model", model);
			glDrawArrays(GL_TRIANGLES, 0, 6);*/
		}

		for (int i = 0; i < 20; i++)
			for (int j = 0; j < 20; j++)
			{
				model = glm::mat4();
				glm::vec3 bunchPosition(-5.f + i / 2.f, 0, -5.f + j / 2.f);
				model = glm::translate(model, bunchPosition);
				DrawBrunchOfGrass(shaderBlending, model);
			}
		//glViewport(0, 0, 1024, 1024);
		////glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
		//glClear(GL_DEPTH_BUFFER_BIT);
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, floorTexture);
		//glEnable(GL_CULL_FACE);
		//glCullFace(GL_FRONT);
		//renderScene(shaderBlending);
		//glCullFace(GL_BACK);
		//glBindFramebuffer(GL_FRAMEBUFFER, 0);
		// Draw vegetation


		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	Cleanup();

	// Clear floor VAO

	// Clear grass VAO

	// glfw: terminate, clearing all previously allocated GLFW resources
	glfwTerminate();
	return 0;
}